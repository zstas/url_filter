## URL Filter ##
This is a utilite for blocking http and https pages. It written with Intel DPDK library for great perfomance (10G and above).

## Usage ##
use as:
sudo ./urlfilter -- **VLAN_IN** **VLAN_OUT**