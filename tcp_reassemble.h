#include <rte_rwlock.h>

#define TCP_PKT_MAX_SIZE	2048
#define TCP_MAX_FLOWS		10

#define TCP_REASSLEMBLE_SUCCESS		-1
#define TCP_REASSLEMBLE_NOT_FOUND	-2
#define TCP_REASSLEMBLE_WRONG_SEQ	-3

struct tcp_reassemble_conf
{
	rte_rwlock_t lock;
	struct rte_hash *flows;
	uint64_t iterate;
};

struct tcp_flow
{
	uint32_t src_ip;
	uint16_t src_port;
	uint32_t dst_ip;
	uint32_t dst_port;
};

struct tcp_reassemble
{
	rte_rwlock_t lock;
	uint32_t next_seq;
	uint16_t offset;
	char data[ TCP_PKT_MAX_SIZE ];
	struct tcp_flow key;
};

void tcp_reassemble_init( void );
int tcp_reassemble_append( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size );
int64_t tcp_reassemble_create( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size );
int64_t tcp_reassemble_check( struct tcp_flow *key );
int64_t tcp_reassemble_pkt_check( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp );
int tcp_reassemble_callback( int (*func)( char *, int ), int ret );
int tcp_reassemble_append( struct ipv4_hdr * , struct tcp_hdr *, char *, int );
int tcp_reassemble_append_http( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size );
void tcp_reassemble_clean( int64_t ret );
int tcp_reassemble_append_https( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size );
