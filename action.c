#include "main.h"

//const char redirect_message[] = "HTTP/1.1 302 Moved Temporarily\r\nLocation: http://www.ufanet.ru/blocking.html/\r\nConnection: close\r\n\r\n";
extern __thread __typeof__( char * ) redirect_message;
extern __thread __typeof__( int ) redirect_message_size;

void swap_mac( struct rte_mbuf *m )
{
	struct ether_hdr *eth_hdr = rte_pktmbuf_mtod( m, struct ether_hdr * );
	struct ether_addr mac;

	ether_addr_copy( &eth_hdr->d_addr, &mac);
	ether_addr_copy( &eth_hdr->s_addr, &eth_hdr->d_addr );
	ether_addr_copy( &mac, &eth_hdr->s_addr );
}

void http_redirect( struct rte_mbuf *m )
{
	struct vlan_hdr *vlan_hdr;
	struct ipv4_hdr *ipv4;
	struct ipv6_hdr *ipv6;
	struct tcp_hdr *tcp;

//	swap_mac( m );

	uint16_t ethertype;
	uint8_t vlan_offset = 0;

	struct ether_hdr *eth_hdr = rte_pktmbuf_mtod( m, struct ether_hdr * );

	ethertype = rte_be_to_cpu_16( eth_hdr->ether_type );


	if( ethertype == ETHER_TYPE_VLAN )
	{
		vlan_offset += 4;

		vlan_hdr = (struct vlan_hdr *)(eth_hdr + 1);
		ethertype = rte_be_to_cpu_16( vlan_hdr->eth_proto );

		if( ethertype == ETHER_TYPE_VLAN )
		{
			vlan_offset += 4;

			vlan_hdr = (struct vlan_hdr *)(eth_hdr + 2);
			ethertype = rte_be_to_cpu_16( vlan_hdr->eth_proto );
		}
	}

	switch( ethertype )
	{
		case ETHER_TYPE_IPv4:
			ipv4 = rte_pktmbuf_mtod_offset( m, struct ipv4_hdr *, sizeof(struct ether_hdr) + vlan_offset );

			uint32_t ip_ad = ipv4->src_addr;
			ipv4->src_addr = ipv4->dst_addr;
			ipv4->dst_addr = ip_ad;

			tcp = rte_pktmbuf_mtod_offset( m, struct tcp_hdr *, sizeof(struct ether_hdr) + vlan_offset + sizeof(struct ipv4_hdr) );
			tcp->data_off = 0x50;

			uint16_t t_p = tcp->src_port;
			tcp->src_port = tcp->dst_port;
			tcp->dst_port = t_p;

			uint32_t seq = tcp->sent_seq;
			tcp->sent_seq = tcp->recv_ack;
			tcp->recv_ack = seq;

			tcp->tcp_flags |= 0x11;

			char *payload = (char *)tcp + ( ( tcp->data_off & 0xF0 ) >> 2 );

			uint8_t hdrs_len = payload - (char*)( m->buf_addr ) - m->data_off;
			m->pkt_len = hdrs_len + redirect_message_size - 1;
			m->data_len = hdrs_len + redirect_message_size - 1;
			ipv4->total_length = rte_cpu_to_be_16( m->pkt_len - sizeof(struct ether_hdr) - vlan_offset );
			ipv4->packet_id += 1;
			rte_memcpy( payload, redirect_message, redirect_message_size );

			m->l2_len = sizeof(struct ether_hdr) + vlan_offset;
			m->l3_len = sizeof(struct ipv4_hdr);
			m->l4_len = 20;

			m->ol_flags = PKT_TX_IPV4 | PKT_TX_TCP_CKSUM;

			ipv4->hdr_checksum = 0;
			tcp->cksum = 0;
			tcp->cksum = rte_ipv4_udptcp_cksum( ipv4, tcp );
//			tcp->cksum = rte_ipv4_phdr_cksum(ipv4, 0 );
			ipv4->hdr_checksum = rte_ipv4_cksum( ipv4 );
			break;

		case ETHER_TYPE_IPv6:
			ipv6 = rte_pktmbuf_mtod_offset( m, struct ipv6_hdr *, sizeof(struct ether_hdr) + vlan_offset );

			FILE *fp;
			fp = fopen("dump.txt", "a");
			rte_pktmbuf_dump(fp, m, 1000);
			uint8_t ip6_ad[16];
			rte_memcpy( (void*)ip6_ad, (const void*)ipv6->src_addr, sizeof( ip6_ad) );
			rte_memcpy( (void*)ipv6->src_addr, (const void*)ipv6->dst_addr, sizeof( ip6_ad ) );
			rte_memcpy( (void*)ipv6->dst_addr, (const void*)ip6_ad, sizeof( ip6_ad ) );

			tcp = rte_pktmbuf_mtod_offset( m, struct tcp_hdr *, sizeof(struct ether_hdr) + vlan_offset + sizeof(struct ipv6_hdr) );
			tcp->data_off = 0x50;

			uint16_t t6_p = tcp->src_port;
			tcp->src_port = tcp->dst_port;
			tcp->dst_port = t6_p;

			uint32_t seq6 = tcp->sent_seq;
			tcp->sent_seq = tcp->recv_ack;
			tcp->recv_ack = seq6;

			tcp->tcp_flags |= 0x11;

			char *payload6 = (char *)tcp + ( ( tcp->data_off & 0xF0 ) >> 2 );

			uint8_t hdrs_len6 = payload6 - (char*)( m->buf_addr ) - m->data_off;
			m->pkt_len = hdrs_len6 + redirect_message_size - 1;
			m->data_len = hdrs_len6 + redirect_message_size - 1;
			ipv6->payload_len = rte_cpu_to_be_16( m->pkt_len - sizeof(struct ether_hdr) - vlan_offset - sizeof(struct ipv6_hdr) );
			//ipv4->packet_id += 1;
			rte_memcpy( payload6, redirect_message, redirect_message_size );

			m->l2_len = sizeof(struct ether_hdr) + vlan_offset;
			m->l3_len = sizeof(struct ipv6_hdr);
			m->l4_len = ( tcp->data_off & 0xf0 ) >> 2;

//			m->ol_flags = PKT_TX_IPV6 | PKT_TX_TCP_CKSUM | PKT_TX_TCP_SEG;
			m->ol_flags = PKT_TX_IPV6 | PKT_TX_TCP_CKSUM;
			//m->tso_segsz = ipv6->payload_len - sizeof(struct ipv6_hdr);
			//m->ol_flags = 0;

			//ipv4->hdr_checksum = 0;
			tcp->cksum = 0;
//			tcp->cksum = rte_ipv6_udptcp_cksum( ipv6, tcp );
			tcp->cksum = rte_ipv6_phdr_cksum( ipv6, m->ol_flags );
			fclose(fp);
			break;
	}
}


void tcp_reset( struct rte_mbuf *m )
{
        struct vlan_hdr *vlan_hdr;
        struct ipv4_hdr *ipv4;
        struct tcp_hdr *tcp;

        uint16_t ethertype;
        uint8_t vlan_offset = 0;

        struct ether_hdr *eth_hdr = rte_pktmbuf_mtod( m, struct ether_hdr * );

        ethertype = rte_be_to_cpu_16( eth_hdr->ether_type );

//	swap_mac( m );

        if( ethertype == ETHER_TYPE_VLAN )
        {
                vlan_offset += 4;

                vlan_hdr = (struct vlan_hdr *)(eth_hdr + 1);
                ethertype = rte_be_to_cpu_16( vlan_hdr->eth_proto );

                if( ethertype == ETHER_TYPE_VLAN )
                {
                        vlan_offset += 4;

                        vlan_hdr = (struct vlan_hdr *)(eth_hdr + 2);
                        ethertype = rte_be_to_cpu_16( vlan_hdr->eth_proto );
                }
        }

        switch( ethertype )
        {
                case ETHER_TYPE_IPv4:
                        ipv4 = rte_pktmbuf_mtod_offset( m, struct ipv4_hdr *, sizeof(struct ether_hdr) + vlan_offset );
                        uint32_t ip_ad = ipv4->src_addr;
                        ipv4->src_addr = ipv4->dst_addr;
                        ipv4->dst_addr = ip_ad;

                        tcp = rte_pktmbuf_mtod_offset( m, struct tcp_hdr *, sizeof(struct ether_hdr) + vlan_offset + sizeof(struct ipv4_hdr) );
                        uint16_t t_p = tcp->src_port;
                        tcp->src_port = tcp->dst_port;
                        tcp->dst_port = t_p;
                        tcp->sent_seq = tcp->recv_ack;
                        tcp->recv_ack = 0;
                        tcp->tcp_flags = 0x4;

                        char *payload = (char *)tcp + ( ( tcp->data_off & 0xF0 ) >> 2 );

                        uint8_t hdrs_len = payload - (char*)( m->buf_addr ) - m->data_off;
                        m->pkt_len = hdrs_len;
                        m->data_len = hdrs_len;
                        ipv4->total_length = rte_cpu_to_be_16( m->pkt_len - sizeof(struct ether_hdr) - vlan_offset );

                        m->l2_len = sizeof(struct ether_hdr) + vlan_offset;
                        m->l3_len = sizeof(struct ipv4_hdr);
                        m->l4_len = 20;

                        m->ol_flags = PKT_TX_IPV4 | PKT_TX_TCP_CKSUM;

                        ipv4->hdr_checksum = 0;
                        tcp->cksum = 0;
                        tcp->cksum = rte_ipv4_udptcp_cksum( ipv4, tcp );
//                        tcp->cksum = rte_ipv4_phdr_cksum(ipv4, 0 );
                        ipv4->hdr_checksum = rte_ipv4_cksum( ipv4 );

			break;
	}
}
