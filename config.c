#include "main.h"

#define CONFIG_FILE_NOT_EXIST		-1

#define CONFIG_SECTION_NETFLOW		1
#define CONFIG_SECTION_POOL		2
#define CONFIG_SECTION_CLASSIFIER	3

extern struct app_config global_config;

uint8_t startWith( char *str, const char *pre )
{
        return strncmp( pre, str, strlen( pre ) ) == 0;
}

uint32_t parseIP( char *str )
{
	int ipv4_bytes[4];
	int ret = sscanf( str, "%d.%d.%d.%d", &ipv4_bytes[0], &ipv4_bytes[1], &ipv4_bytes[2], &ipv4_bytes[3] );

	if( ret == 4 )
	{
		uint32_t ipv4 = ( ipv4_bytes[0] << 24 ) + ( ipv4_bytes[1] << 16 ) + ( ipv4_bytes[2] << 8 ) + ipv4_bytes[3];
		return ipv4;
	}
	return 0;
}

uint16_t parse_uint16_t( char *str )
{
	uint16_t result;
	int ret = sscanf( str, "%hu", &result );

	if( ret == 1 )
		return result;

	return 0xFFFF;
}

uint8_t parse_uint8_t( char *str )
{
	uint8_t result;
	int ret = sscanf( str, "%hhu", &result );

	if( ret == 1 )
		return result;

	return 0xFF;
}

char * parse_string( char *str )
{
	char * temp = malloc( strlen( str ) + 1 );
	memset( temp, 0, strlen( str ) + 1 );
	strncpy( temp, str, strlen( str ) - 1);
	return temp;
}

void parse_eal_args( char *str )
{
	char * temp = malloc( strlen( str ) + 1 );
	memset( temp, 0, strlen( str ) + 1 );
	strncpy( temp, str, strlen( str ) - 1);

	char name[12] = "http_block";

	global_config.eal_args_count = 0;
	global_config.eal_args[ global_config.eal_args_count ] = malloc( strlen( name ) + 1 );
	memset( global_config.eal_args[ global_config.eal_args_count ], 0, strlen( name ) + 1 );
	strncpy( global_config.eal_args[ global_config.eal_args_count ], name, strlen( name ) );
	global_config.eal_args_count++;

	char * tok = strtok( temp, " " );
	while( tok != NULL )
	{
		global_config.eal_args[ global_config.eal_args_count ] = malloc( strlen( tok ) + 1 );
		memset( global_config.eal_args[ global_config.eal_args_count ], 0, strlen( tok ) + 1 );
		strncpy( global_config.eal_args[ global_config.eal_args_count ], tok, strlen( tok ) );
		printf( "Parsing string %s\n", global_config.eal_args[ global_config.eal_args_count ] );
		global_config.eal_args_count++;
		tok = strtok( NULL, " " );
	}
}

void prepare_config( void )
{
//	global_config.netflow.template_timer = NETFLOW_TEMPLATE_TIMER;
}

void print_config( void )
{
	printf( "Transmit and receive mode: %x\n", global_config.mode );
	printf( "EAL arguments count: %d\n", global_config.eal_args_count );
	for( int i = 0; i < global_config.eal_args_count; i++ )
	{
		printf( "Argument[%d]: %s ", i, global_config.eal_args[ i ] );
	}
	printf( "\n" );
	printf( "Input vlan: %d\n", global_config.vlan_in );
	printf( "Output vlan: %d\n", global_config.vlan_out );
	printf( "Registry file path: %s\n", global_config.registry );
	printf( "SSLHosts file path: %s\n", global_config.ssl_hosts );
	printf( "Watchlist file path: %s\n", global_config.watchlist );
	printf( "Redirect URL: %s\n", global_config.redirect_url );
}

int load_config( char * file )
{
	FILE * f = fopen( file, "r" );

	if( f == NULL )
		return CONFIG_FILE_NOT_EXIST;

	char line[ 2048 ];
	uint8_t section = 0;

	memset( &global_config, 0, sizeof( global_config ) );
	prepare_config();

	while( 1 )
	{
		memset( line, 0, sizeof( line ) );
		if( fgets( line, sizeof( line ), f ) == NULL )
			break;

		if( startWith( line, "[dpdk]" ) )
		{
			section = CONFIG_SECTION_DPDK;
			continue;
		}

		if( startWith( line, "[filter]" ) )
		{
			section = CONFIG_SECTION_FILTER;
			continue;
		}

		char *key = strtok( line, " =" );
		if( key == NULL )
			continue;

		switch( section )
		{
			case CONFIG_SECTION_DPDK:
				if( startWith( key, "eal_args" ) )
				{
					char *value = strtok( NULL, "=" );
					parse_eal_args( value );
				}
				if( startWith( key, "mode" ) )
				{
					char *value = strtok( NULL, " =" );
					global_config.mode = parse_uint8_t( value );
				}
				if( startWith( key, "vlan_in" ) )
				{
					char *value = strtok( NULL, " =" );
					global_config.vlan_in = parse_uint16_t( value );
				}
				if( startWith( key, "vlan_out" ) )
				{
					char *value = strtok( NULL, " =" );
					global_config.vlan_out = parse_uint16_t( value );
				}
				break;
			case CONFIG_SECTION_FILTER:
				if( startWith( key, "registry" ) )
				{
					char *value = strtok( NULL, " =" );
					global_config.registry = parse_string( value );
				}
				if( startWith( key, "ssl_hosts" ) )
				{
					char *value = strtok( NULL, " =" );
					global_config.ssl_hosts = parse_string( value );
				}
				if( startWith( key, "watchlist" ) )
				{
					char *value = strtok( NULL, " =" );
					global_config.watchlist = parse_string( value );
				}
				if( startWith( key, "redirect_url" ) )
				{
					char *value = strtok( NULL, " =" );
					global_config.redirect_url = parse_string( value );
				}
				break;
			default:
				break;
		}

	}

	print_config();

	fclose( f );

	return 0;
}
