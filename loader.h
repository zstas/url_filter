uint8_t startWith( char *str, const char *pre );
void to_lower( char *str );
void stripRight( char* str, char c );
void loader_init( const char *filename );
void sslhost_load( const char *filename );
void watchlist_load( const char *filename );
