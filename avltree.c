#include "main.h"

//extern int strcmp_sse42_64(const char *, const char *);
__thread __typeof__( int ) cmp_res;

static int node_cmp( struct node *n, char *k )
{
	uint32_t len = strlen( k );
	if( n->len > len )
		len = n->len;

	RTE_LOG( DEBUG, WORKER , "node_cmp: %s %s len %d\n", n->key, k, len );
	return memcmp( n->key, k, len );
}

static int node_cmp_trim( struct node *n, char *k )
{
	RTE_LOG( DEBUG, WORKER ,  "node_cmp_trim: %s %s len %d\n", n->key, k, n->len );
	return memcmp( n->key, k, n->len );
}

static struct node* init_node( char *k )
{
	struct node *n = rte_malloc( "struct_node", sizeof( struct node ), 0 );

	n->len = strlen( k );

	n->key = rte_malloc( "struct_node_key", n->len, 0 );
	memcpy( n->key, k, n->len );

	n->left = NULL;
	n->right = NULL;
	n->height = 1;

	return n;
}

static unsigned char height( struct node *p )
{
	return p?p->height:0;
}

static int bfactor( struct node* p )
{
	return height( p->right ) - height( p->left );
}

static void fixheight( struct node* p )
{
	unsigned char hl = height(p->left);
	unsigned char hr = height(p->right);
	p->height = (hl>hr?hl:hr)+1;
}

static struct node* rotateright( struct node* p) // правый поворот вокруг p
{
	struct node* q = p->left;
	p->left = q->right;
	q->right = p;
	fixheight(p);
	fixheight(q);
	return q;
}

static struct node* rotateleft( struct node* q) // левый поворот вокруг q
{
	struct node* p = q->right;
	q->right = p->left;
	p->left = q;
	fixheight(q);
	fixheight(p);
	return p;
}

static struct node* balance( struct node* p) // балансировка узла p
{
	fixheight(p);
	if( bfactor(p)==2 )
	{
		if( bfactor(p->right) < 0 )
			p->right = rotateright(p->right);
		return rotateleft(p);
	}
	if( bfactor(p)==-2 )
	{
		if( bfactor(p->left) > 0  )
			p->left = rotateleft(p->left);
		return rotateright(p);
	}
	return p; // балансировка не нужна
}

struct node* avl_insert( struct node** p, char *k ) // вставка ключа k в дерево с корнем p
{
	if( (*p) == NULL )
	{
		(*p) = init_node( k );
		return (*p);
	}

//	printf( "INSERT key %s in node %s\n", k, (*p)->key );

	int res = node_cmp_trim( (*p), k );

	if( res > 0 )
		(*p)->left = avl_insert( &((*p)->left),k );
	else
		(*p)->right = avl_insert( &((*p)->right),k );

	return balance( (*p) );
}

static struct node* findmin( struct node* p ) // поиск узла с минимальным ключом в дереве p
{
	if( p == NULL )
	{
		//printf( "findmin: current node is null\n" );
		return NULL;
	}

	if( p->left == NULL )
		return p;
	else
		return findmin( p->left );
}

struct node* avl_lookup( struct node* p, char *k ) // поиск узла с минимальным ключом в дереве p
{
	if( p == NULL )
		return NULL;

	int res = node_cmp( p, k );
//	printf( "CMP for node key %s and key %s with res %d\n", p->key, k, res );

	if( res > 0 )
		return avl_lookup( p->left, k );
	if( res < 0 )
		return avl_lookup( p->right, k );
	if( res == 0 )
		return p;

	return NULL;
}

struct node* avl_lookup_trim( struct node* p, char *k ) // поиск узла с минимальным ключом в дереве p
{
	if( p == NULL )
		return NULL;

	cmp_res = node_cmp_trim( p, k );
//	printf( "CMP trim for node key %s and key %s with res %d\n", p->key, k, res );

	if( cmp_res > 0 )
		return avl_lookup_trim( p->left, k );
	if( cmp_res < 0 )
		return avl_lookup_trim( p->right, k );
	if( cmp_res == 0 )
		return p;

	return NULL;
}

static struct node* removemin( struct node* p ) // удаление узла с минимальным ключом из дерева p
{
	if( p->left==0 )
		return p->right;

	p->left = removemin(p->left);

	return balance(p);
}

static struct node* remove_node( struct node* p, char *k ) // удаление ключа k из дерева p
{
	if( p == NULL ) return 0;

	int res = node_cmp( p, k );

	if( res < 0 )
		p->left = remove_node(p->left,k);
	else if( res > 0 )
		p->right = remove_node(p->right,k);
	else //  k == p->key
	{
		struct node* q = p->left;
		struct node* r = p->right;
		rte_free( p->key );
		rte_free( p );
		if( !r ) return q;
		struct node* min = findmin(r);
		min->right = removemin(r);
		min->left = q;
		return balance(min);
	}
	return balance(p);
}

void avl_print_node( struct node* n )
{
	if( n == NULL )
		return;

	RTE_LOG( DEBUG, WORKER , "Key: %s len: %d height: %d\n", n->key, n->len, n->height );
	avl_print_node( n->left );
	avl_print_node( n->right );
}

void avl_remove_tree( struct node** n )
{
	if( *n == NULL )
		return;

//	printf( "trying to remove node %s with height %d, left %p right %p\n", (*n)->key, (*n)->height, (*n)->left, (*n)->right );
	if( (*n)->left != NULL )
		avl_remove_tree( &( (*n)->left ) );
	if( (*n)->right != NULL )
		avl_remove_tree( &( (*n)->right ) );
	if( (*n)->key != NULL )
		rte_free( (*n)->key );
	rte_free( *n );
	*n = NULL;
}

