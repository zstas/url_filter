#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/signal.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/if.h>
#include <linux/if_tun.h>

#include <rte_common.h>
#include <rte_byteorder.h>
#include <rte_log.h>
#include <rte_malloc.h>
#include <rte_memory.h>
#include <rte_memcpy.h>
#include <rte_memzone.h>
#include <rte_eal.h>
#include <rte_launch.h>
#include <rte_ring.h>
#include <rte_mempool.h>
#include <rte_mbuf.h>
#include <rte_ethdev.h>
#include <rte_ip.h>
#include <rte_tcp.h>
#include <rte_udp.h>
#include <rte_icmp.h>
#include <rte_hash.h>
#include <rte_hash_crc.h>
#include <rte_jhash.h>

#include "config.h"
#include "worker.h"
#include "lcore.h"
#include "lcore_tx.h"
#include "lcore_rx.h"
#include "lcore_txrx.h"
#include "lcore_tap.h"
#include "loader.h"
#include "tcp_reassemble.h"
#include "action.h"
#include "list.h"
#include "avltree.h"
#include "registry.h"
#include "https.h"
#include "dpi_http.h"
#include "port.h"

#define VERSION "v2.0.1"

#define RX_RING_SIZE 512
#define TX_RING_SIZE 1024

#define NUM_MBUFS	8192
#define MBUF_CACHE_SIZE 250
#define BURST_SIZE	64

#define MSG_POOL_SIZE	1024
#define MSG_MAX_SIZE	1024

#define RTE_LOGTYPE_LCORE_RX	RTE_LOGTYPE_USER1
#define RTE_LOGTYPE_LCORE_TX	RTE_LOGTYPE_USER2
#define RTE_LOGTYPE_LCORE_TXRX	RTE_LOGTYPE_USER3
#define RTE_LOGTYPE_WORKER	RTE_LOGTYPE_USER4
#define RTE_LOGTYPE_MAIN	RTE_LOGTYPE_USER5
#define RTE_LOGTYPE_LCORE_TAP	RTE_LOGTYPE_USER6

#define ONE_TXRX	0x1
#define MANY_TXRX	0x2
#define ONE_TX_ONE_RX	0x3
#define MANY_TX_MANY_RX	0x4

struct app_config
{
	uint8_t workers_count;
	uint8_t rxs_count;

	struct lcore_conf *lcore_worker;
	struct lcore_conf *lcore_rx;

	uint8_t mode;
	char *redirect_url;
	uint16_t vlan_in;
	uint16_t vlan_out;
	char *registry;
	char *ssl_hosts;
	char *watchlist;
	char *eal_args[ 16 ];
	int eal_args_count;
};

struct ipc_msg
{
        uint8_t type;
        uint8_t data[ IPC_MSG_SIZE ];
};

void lcore_main( void );
void print_version( void );
int parse_args( int argc, char ** argv );
void print_help( void );
int schedule_lcores( uint8_t lcore_sched_scheme );
void init_ipc_to_lcores( void );
void run_worker( uint8_t id );
void stop_worker( uint8_t id );
void reload_worker( uint8_t id );
void reload_all_workers( void );
void reload_handler( int sig );
