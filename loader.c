#include "main.h"

extern __thread __typeof__( struct registry_entry** ) http_registry;
extern __thread __typeof__( struct rte_hash* ) ipv4_registry_hash;
extern __thread __typeof__( struct rte_hash* ) http_registry_hash;
extern __thread __typeof__( struct rte_hash* ) sslhost_registry_hash;
extern __thread __typeof__( struct rte_hash* ) watchlist_hash;

void to_lower( char *str )
{
	int i = 0;
	while( str[i] )
	{
		str[i] = tolower( str[i] );
		i += 1;
	}
}

void stripRight( char* str, char c )
{
	if( str[ strlen( str ) - 1 ] == c )
		str[ strlen( str ) - 1 ] = 0x0;
}

void loader_init( const char *filename )
{
	FILE *fp;
	fp = fopen( filename, "r" );

	char line[ 2048 ];
	char hostname[ 1024 ], uri [ 1024 ];

	if( fp == NULL )
		return;

	int32_t index = 0;

	while( 1 )
	{
		memset( line, 0, sizeof( line ) );
		memset( hostname, 0, sizeof( hostname ) );
		memset( uri, 0, sizeof( uri ) );

		if( fgets( line, sizeof( line ), fp ) == NULL )
			break;

		if( startWith( line, "domain://" ) )
		{
			int wildcard_blocked = 0;

			sscanf( line, "domain://%1023s\n", hostname );

			RTE_LOG( DEBUG, WORKER , "Parsed a domain: %s\n", hostname );
			RTE_LOG( DEBUG, WORKER , "line is: %s\n", line );

			if( startWith( hostname, "*." ) )
			{
				wildcard_blocked = 1;
				strncpy( hostname, hostname+2, 1020 );
			}

			to_lower( hostname );
			stripRight( hostname, '.' );

			index = rte_hash_lookup( http_registry_hash, hostname );

			if( index < 0 )
			{
				index = rte_hash_add_key( http_registry_hash, hostname );
				regentry_init( &( http_registry[ index ] ), hostname, 256 );

			}

			RTE_LOG( DEBUG, WORKER, "Making %s totally block for uri %s cause domain\n", hostname, uri );
			make_totally_blocked( &( http_registry[ index ] ) );
			regentry_block_https( &( http_registry[ index ] ) );

			if( wildcard_blocked )
			{
				make_wildcard_blocked( &( http_registry[ index ] ) );
			}

			continue;
		}

		if( startWith( line, "http://" ) )
		{
			if( sscanf( line, "http://%1023[^/]%1023s\n", hostname, uri ) == 1 )
			{
				hostname[ strlen( hostname ) - 1 ] = 0x0;
			}

			to_lower( hostname );
			stripRight( hostname, '.' );

			char *offset = strstr( uri, "#" );
			if( offset != NULL )
			{
				int off_len = (int)( offset - uri );
				memset( offset, 0, sizeof( uri ) - off_len );
			}

			index = rte_hash_lookup( http_registry_hash, hostname );
			if( index >= 0 )
			{
				regentry_add_url( http_registry[ index ], uri  );
				RTE_LOG( DEBUG, WORKER, "Adding uri to existing site: %s; uri: %s\n", hostname, uri );
			}
			else
			{
				index = rte_hash_add_key( http_registry_hash, hostname );
				RTE_LOG( DEBUG, WORKER, "Adding new site: %s; uri: %s\n", hostname, uri );
				regentry_init( &( http_registry[ index ] ), hostname, 256 );
				regentry_add_url( http_registry[ index ], uri );
			}

			if( uri[0] == 0x0 || uri[0] == ' ' )
			{
				RTE_LOG( DEBUG, WORKER, "Making %s totally block for uri %s cause 1\n", hostname, uri );
				make_totally_blocked( &( http_registry[ index ] ) );
			}

			if( ( strlen( uri ) == 1 ) && ( uri[0] == '/' ) )
			{
				RTE_LOG( DEBUG, WORKER, "Making %s totally block for uri %s cause 1\n", hostname, uri );
				make_totally_blocked( &( http_registry[ index ] ) );
			}

			continue;
		}

		if( startWith( line, "https://" ) )
		{
			if( sscanf( line, "https://%1023[^/]%1023s\n", hostname, uri ) >= 1 )
			{
				to_lower( hostname );
				stripRight( hostname, '.' );

				index = rte_hash_lookup( http_registry_hash, hostname );

				if( index >= 0 )
				{
					regentry_block_https( &( http_registry[ index ] ) );
				}
				else
				{
					index = rte_hash_add_key( http_registry_hash, hostname );
					regentry_init( &( http_registry[ index ] ), hostname, 256 );
					regentry_block_https( &( http_registry[ index ] ) );
				}
			}

			continue;
		}

		if( startWith( line, "ipv4://" ) )
		{
			int ipv4_bytes[4];
			int ret = sscanf( line, "ipv4://%d.%d.%d.%d", &ipv4_bytes[0], &ipv4_bytes[1], &ipv4_bytes[2], &ipv4_bytes[3] );

			if( ret == 4 )
			{
				uint32_t ipv4_key = ( ipv4_bytes[0] << 24 ) + ( ipv4_bytes[1] << 16 ) + ( ipv4_bytes[2] << 8 ) + ipv4_bytes[3];
				rte_hash_add_key( ipv4_registry_hash, &ipv4_key );
				RTE_LOG( DEBUG, WORKER , "Parsed an IPv4 address in registry: %x\n", ipv4_key );
			}

			continue;
		}
	}

	fclose( fp );
}

void sslhost_load( const char *filename )
{
	FILE *fp;
	fp = fopen( filename, "r" );

	if( fp == NULL )
		return;

	char line[ 8192 ];

	int ipv4_bytes[4];

	while( 1 )
	{
		memset( line, 0, sizeof( line ) );

		if( fgets( line, sizeof( line ), fp ) == NULL )
			break;

		int ret = sscanf( line, "%d.%d.%d.%d", &ipv4_bytes[0], &ipv4_bytes[1], &ipv4_bytes[2], &ipv4_bytes[3] );
		if( ret == 4 )
		{
			uint32_t ipv4_key = ( ipv4_bytes[0] << 24 ) + ( ipv4_bytes[1] << 16 ) + ( ipv4_bytes[2] << 8 ) + ipv4_bytes[3];
			rte_hash_add_key( sslhost_registry_hash, &ipv4_key );
			RTE_LOG( DEBUG, WORKER , "Parsed an IPv4 address in sslhost: %x\n", ipv4_key );
		}
	}

	fclose( fp );
}

void watchlist_load( const char *filename )
{
	FILE *fp;
	fp = fopen( filename, "r" );

	if( fp == NULL )
		return;

	char line[ 8192 ];
	char hostname[ 1024 ];

	while( 1 )
	{
		memset( line, 0, sizeof( line ) );
		memset( hostname, 0, sizeof( hostname ) );

		if( fgets( line, sizeof( line ), fp ) == NULL )
			break;

		if( startWith( line, "domain://" ) )
		{
			if( sscanf( line, "domain://%1023s\n", hostname ) == 1 )
			{
				RTE_LOG( INFO, WORKER , "Parsed a domain for watching list: %s\n", hostname );
				//hostname[ strlen( hostname ) - 1 ] = 0x0;
			}

			to_lower( hostname );
			stripRight( hostname, '.' );

			if( rte_hash_lookup( watchlist_hash, hostname ) < 0 )
				rte_hash_add_key( watchlist_hash, hostname );
		}
	}

	fclose( fp );
}
