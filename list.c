#include "main.h"

void init_list( struct list **nl, char *t, size_t length )
{
	*nl = ( struct list* )rte_malloc( "struct_list", sizeof( struct list ), 0 );
	struct list *l = *nl;
	l->next = NULL;
	l->text = ( char* )rte_malloc( "list_name", length + 1, 0 );
	strncpy( l->text, t, length );
	l->text[ length ] = 0x0;
	l->len = strlen( t );
}

void insert( struct list **nl, char *t, size_t length )
{
	struct list *l = *nl;
	while( l->next != NULL )
	{
		l = l->next;
	}
	init_list( &( l->next ), t, length );
}

void print_list( struct list *l )
{
	printf( "printing list\n" );
	do
	{
		printf( "Item: %s ptr %p\n", l->text, l->next );
		l = l->next;
	} while( l != NULL );
}

void flush_list( struct list **nl )
{
	if( nl == NULL )
		return;

	struct list *l = *nl;
	struct list *new_l;
	if( l == NULL )
		return;
	while( l->next != NULL )
	{
		rte_free( l->text );
		new_l = l;
		l = l->next;
		rte_free( new_l );
	}
	rte_free( l->text );
	rte_free( l );
	*nl = NULL;
}
