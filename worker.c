#include "main.h"

extern struct app_config global_config;
__thread __typeof__( struct lcore_conf ) lcore;

extern __thread __typeof__( char ) host[4096];
extern __thread __typeof__( char )  uri[4096];
extern __thread __typeof__( struct ipv4_hdr * ) ipv4;
extern __thread __typeof__( struct ipv6_hdr * ) ipv6;
extern __thread __typeof__( struct tcp_hdr * ) tcp;
extern __thread __typeof__( uint8_t ) worker_id;

__thread __typeof__( char * ) redirect_message;
__thread __typeof__( int ) redirect_message_size;

void
worker_process_messages( void )
{
        struct ipc_msg *msg;

        if( rte_ring_dequeue( lcore.from_main, ( void ** )&msg ) != 0 )
                return;

	RTE_LOG( INFO, WORKER , "Got a ipc message with type 0x%x\n", msg->type );
	switch( msg->type )
	{
		case IPC_WORKER_RELOAD:
			RTE_LOG( INFO, WORKER, "Worker %d got msg RELOAD\n", worker_id );
			setup_hash( global_config.registry, global_config.ssl_hosts, global_config.watchlist );
			msg->type = IPC_WORKER_RESPONSE;
			rte_ring_enqueue( lcore.to_main, ( void * )msg );
			break;
		default:
			RTE_LOG( INFO, WORKER , "Worker %d got unknown message with type 0x%x\n", worker_id, msg->type );
			rte_free( msg );
	}
}

int lcore_worker( void *w_id )
{
	worker_id = *( uint8_t* )( w_id );
	RTE_LOG( DEBUG, WORKER , "Launching worker %d...\n", worker_id );
	redirect_message = rte_malloc( NULL, 255, 0);
	snprintf( redirect_message, 255, "HTTP/1.1 302 Moved Temporarily\r\nLocation: %s\r\nConnection: close\r\n\r\n", global_config.redirect_url );
	redirect_message_size = strlen( redirect_message );

	lcore_worker_init_conf( &lcore, LCORE_WORKER, worker_id );

	create_hashes();
	setup_hash( global_config.registry, global_config.ssl_hosts, global_config.watchlist );

	char buf[128];
	memset( buf, 0, 128);
	snprintf( buf, 128, "worker_%d", worker_id );

	struct rte_ring * ring;
	ring = rte_ring_create( buf, WORKER_RING_SIZE, rte_socket_id(), RING_F_SC_DEQ );

/*
	struct rte_ring * tap_ring;
	memset( buf, 0, 128);
	snprintf( buf, 128, "tap_tx" );

	while( rte_ring_lookup( buf ) == NULL )
	{
		sleep(1);
		RTE_LOG(INFO, WORKER , "Waiting for ring tap_tx\n" );
	}

	tap_ring = rte_ring_lookup( buf );
*/
	uint8_t nb_ports = rte_eth_dev_count_avail();
	uint8_t port = 0;

	struct rte_ring *ring_tx[10];

	for( port = 0; port < nb_ports; port += 1 )
	{
		memset( buf, 0, 128);
		snprintf( buf, 128, "port_%d_tx", port );
		while( rte_ring_lookup( buf ) == NULL )
		{
			sleep(1);
			RTE_LOG( INFO, WORKER , "Waiting for ring tx port %d\n", port );
		}

                ring_tx[ port ] = rte_ring_lookup( buf );
	}

	RTE_LOG( INFO, WORKER , "All preparation for worker %d is done. Start processing\n", worker_id );

	void *packets[32];
	struct rte_mbuf *pkt;
	uint16_t rx_pkts = 32;

	void *tx_packets_0[32];
	void *tx_packets_1[32];
	uint16_t tx_pkts_0 = 0;
	uint16_t tx_pkts_1 = 0;

	uint8_t i;
	uint32_t j = 0;

	while( 1 )
	{
		rx_pkts = rte_ring_dequeue_burst( ring, packets, 32, NULL );
		memset( tx_packets_0, 0, 32 );
		memset( tx_packets_1, 0, 32 );
		tx_pkts_0 = 0;
		tx_pkts_1 = 0;


		for( i = 0; i < rx_pkts; i += 1 )
		{
			RTE_LOG( DEBUG, WORKER, "Worker %d got %d pkts\n", worker_id, rx_pkts );

			pkt = (struct rte_mbuf*)packets[i];
			rte_prefetch0( rte_pktmbuf_mtod( pkt, void * ) );

			switch( classify_pkt( pkt ) )
			{
				case DROP:
					RTE_LOG( INFO, WORKER, "Drop packet with dst ip " IPv4_BYTES_FMT "\n", IPv4_BYTES( rte_be_to_cpu_32( ipv4->dst_addr ) ) );
					rte_pktmbuf_free( pkt );
					break;
				case REDIRECT:
//					RTE_LOG( INFO, WORKER, "ID%d " IPv4_BYTES_FMT ":%d:" IPv4_BYTES_FMT ":%d -> Redirect for http://%s%s\n", worker_id, IPv4_BYTES( rte_be_to_cpu_32( ipv4->src_addr ) ), rte_be_to_cpu_16( tcp->src_port ), IPv4_BYTES( rte_be_to_cpu_32( ipv4->dst_addr ) ), rte_be_to_cpu_16( tcp->dst_port ), host, uri );
//					swap_mac( pkt );
					http_redirect( pkt );
					//rte_pktmbuf_dump( stdout, pkt, 512 );
//					rte_ring_enqueue( ring_tx[ pkt->port ], pkt );
					if( pkt->port == 0 ) tx_packets_0[ tx_pkts_0++ ] = pkt; else tx_packets_1[ tx_pkts_1++ ] = pkt;
					//rte_ring_enqueue( tap_ring, pkt );
					//rte_pktmbuf_free( pkt );
					break;
				case REDIRECT_V6:
					RTE_LOG( INFO, WORKER, "ID%d " IPv6_BYTES_FMT ":%d:" IPv6_BYTES_FMT ":%d -> Redirect for http://%s%s\n", worker_id, IPv6_BYTES( ipv6->src_addr ), rte_be_to_cpu_16( tcp->src_port ), IPv6_BYTES( ipv6->dst_addr ), rte_be_to_cpu_16( tcp->dst_port ), host, uri );
//					swap_mac( pkt );
					http_redirect( pkt );
					//rte_pktmbuf_dump( stdout, pkt, 512 );
//					rte_ring_enqueue( ring_tx[ pkt->port ], pkt );
					if( pkt->port == 0 ) tx_packets_0[ tx_pkts_0++ ] = pkt; else tx_packets_1[ tx_pkts_1++ ] = pkt;
					//rte_ring_enqueue( tap_ring, pkt );
					//rte_pktmbuf_free( pkt );
					break;
				case RESET:
//					RTE_LOG( INFO, WORKER, "ID%d " IPv4_BYTES_FMT ":%d:" IPv4_BYTES_FMT ":%d -> Send TCP reset for %s\n", worker_id, IPv4_BYTES( rte_be_to_cpu_32( ipv4->src_addr ) ), rte_be_to_cpu_16( tcp->src_port ), IPv4_BYTES( rte_be_to_cpu_32( ipv4->dst_addr ) ), rte_be_to_cpu_16( tcp->dst_port ), host );
//					swap_mac( pkt );
					tcp_reset( pkt );
//					rte_ring_enqueue( ring_tx[ pkt->port ], pkt );
					if( pkt->port == 0 ) tx_packets_0[ tx_pkts_0++ ] = pkt; else tx_packets_1[ tx_pkts_1++ ] = pkt;
					//rte_ring_enqueue( tap_ring, pkt );
					//rte_pktmbuf_free( pkt );
					break;
				case RESET_IP:
					RTE_LOG( INFO, WORKER, "ID%d " IPv4_BYTES_FMT ":%d:" IPv4_BYTES_FMT ":%d -> Send TCP reset cause IP\n", worker_id, IPv4_BYTES( rte_be_to_cpu_32( ipv4->src_addr ) ), rte_be_to_cpu_16( tcp->src_port ), IPv4_BYTES( rte_be_to_cpu_32( ipv4->dst_addr ) ), rte_be_to_cpu_16( tcp->dst_port ) );
//					swap_mac( pkt );
					tcp_reset( pkt );
//					rte_ring_enqueue( ring_tx[ pkt->port ], pkt );
					if( pkt->port == 0 ) tx_packets_0[ tx_pkts_0++ ] = pkt; else tx_packets_1[ tx_pkts_1++ ] = pkt;
					//rte_pktmbuf_free( pkt );
					break;
//				case TAP_REDIRECT:
//					RTE_LOG( INFO, WORKER, "Sending redirect over tap interface for %s\n", host );
//					http_redirect( pkt );
					//rte_ring_enqueue( tap_ring, pkt );
//					rte_pktmbuf_free( pkt );
//					break;
				default:
//					RTE_LOG(DEBUG, WORKER , "Pass\n" );
//					swap_mac( pkt );
//					rte_ring_enqueue( ring_tx[ pkt->port ], pkt );
					if( pkt->port == 0 ) tx_packets_0[ tx_pkts_0++ ] = pkt; else tx_packets_1[ tx_pkts_1++ ] = pkt;
					//rte_pktmbuf_free( pkt );
					break;
			}
		}

		rte_ring_enqueue_bulk( ring_tx[1], tx_packets_0, tx_pkts_0, NULL );
		rte_ring_enqueue_bulk( ring_tx[0], tx_packets_1, tx_pkts_1, NULL );

		if( unlikely( j++ == 10000 ) )
		{
			worker_process_messages();
			j = 0;
			//RTE_LOG( INFO, WORKER, "j10000, worker %d", worker_id );
		}
	}

	//setup_hash( worker_id, "blocked.lst" );
	return 0;
}
