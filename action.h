char* get_payload( struct rte_mbuf *m );
void http_redirect( struct rte_mbuf *m );
void tcp_reset( struct rte_mbuf *m );
void swap_mac( struct rte_mbuf *m );

