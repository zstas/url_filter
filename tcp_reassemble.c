#include "main.h"

#ifndef IPv4_BYTES
#define IPv4_BYTES_FMT "%" PRIu8 ".%" PRIu8 ".%" PRIu8 ".%" PRIu8
#define IPv4_BYTES(addr) \
                (uint8_t) (((addr) >> 24) & 0xFF),\
                (uint8_t) (((addr) >> 16) & 0xFF),\
                (uint8_t) (((addr) >> 8) & 0xFF),\
                (uint8_t) ((addr) & 0xFF)
#endif

//__thread __typeof__( struct rte_hash* ) flows;
struct tcp_reassemble_conf tcp_reass_conf;
struct tcp_reassemble * reassemble_packets;
extern __thread __typeof__( uint8_t ) worker_id;

void tcp_reassemble_init( void )
{
	uint32_t mz_size = sizeof( struct tcp_reassemble ) * TCP_MAX_FLOWS;
	const struct rte_memzone *mz;
	mz = rte_memzone_reserve( "tcp_reassemble", mz_size, rte_socket_id(), RTE_MEMZONE_SIZE_HINT_ONLY | RTE_MEMZONE_1GB | RTE_MEMZONE_2MB   );
	if( mz == NULL )
		rte_exit( EXIT_FAILURE, "Cannot reserve memory for reassemble packet table.\n" );

	reassemble_packets = ( struct tcp_reassemble * )mz->addr;

	RTE_LOG( DEBUG, MAIN, "ack: %d offset: %d data: %d\n", reassemble_packets[0].next_seq,  reassemble_packets[0].offset,  reassemble_packets[0].data[0] );

	rte_rwlock_init( &tcp_reass_conf.lock );
	tcp_reass_conf.iterate = 0;

	for( int i = 0; i < TCP_MAX_FLOWS; i++ )
	{
		rte_rwlock_init( &reassemble_packets[i].lock );
	}

        struct rte_hash_parameters hash_params =
        {
                .name = "tcp_reassemble",
                .entries = TCP_MAX_FLOWS,
                .key_len = sizeof( struct tcp_flow ),
                .hash_func_init_val = 0,
        };

	tcp_reass_conf.flows = rte_hash_create( &hash_params );
}

int64_t tcp_reassemble_pkt_check( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp )
{
	RTE_LOG( DEBUG, WORKER, "Lookup in reassemble table: " IPv4_BYTES_FMT ":%d:" IPv4_BYTES_FMT ":%d\n", IPv4_BYTES( rte_be_to_cpu_32( ipv4->src_addr ) ), rte_be_to_cpu_16( tcp->src_port ), IPv4_BYTES( rte_be_to_cpu_32( ipv4->dst_addr ) ), rte_be_to_cpu_16( tcp->dst_port ) );
        struct tcp_flow key;
        key.src_ip = ipv4->src_addr;
        key.dst_ip = ipv4->dst_addr;
        key.src_port = tcp->src_port;
        key.dst_port = tcp->dst_port;

	return tcp_reassemble_check( &key );
}

int64_t tcp_reassemble_check( struct tcp_flow *key )
{
	RTE_LOG( DEBUG, WORKER, "Entering tcp_reassemble_check\n" );
	int64_t index;
	rte_rwlock_write_lock( &tcp_reass_conf.lock );
	int ret = rte_hash_lookup_data( tcp_reass_conf.flows, &key, (void**)&index );
	rte_rwlock_write_unlock( &tcp_reass_conf.lock );
	RTE_LOG( DEBUG, WORKER, "ID%d " IPv4_BYTES_FMT ":%d:" IPv4_BYTES_FMT ":%d checked for reassembly with result %d\n", worker_id,
				       IPv4_BYTES( rte_be_to_cpu_32( key->src_ip ) ), rte_be_to_cpu_16( key->src_port ),
				       IPv4_BYTES( rte_be_to_cpu_32( key->dst_ip ) ), rte_be_to_cpu_16( key->dst_port ), ret );
	if( ret >= 0 )
	{
		RTE_LOG( DEBUG, WORKER, "Lookup in reassemble table result: %ld\n", index );
		return index;
	}
	else
	{
		RTE_LOG( DEBUG, WORKER, "Lookup in reassemble table result failed\n" );
		return -1;
	}
}

int tcp_reassemble_append( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size )
{
	RTE_LOG( DEBUG, WORKER, "Entering tcp_reassemble_append\n" );
	RTE_LOG( DEBUG, WORKER, "Trying to reassemble pkt\n" );
	struct tcp_flow key;
	key.src_ip = ipv4->src_addr;
	key.dst_ip = ipv4->dst_addr;
	key.src_port = tcp->src_port;
	key.dst_port = tcp->dst_port;
	int64_t index = 0;

	rte_rwlock_write_lock( &tcp_reass_conf.lock );
	int ret = rte_hash_lookup_data( tcp_reass_conf.flows, &key, (void**)&index );

	if( ret < 0 )
	{
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		return TCP_REASSLEMBLE_NOT_FOUND;
	}
	RTE_LOG( DEBUG, WORKER, "Found flow %ld\n", index );

	RTE_LOG( DEBUG, WORKER, "CMP for seq %" PRIu32 " and %"  PRIu32 "\n", reassemble_packets[ index ].next_seq, rte_be_to_cpu_32( tcp->sent_seq ) );
	if( reassemble_packets[ index ].next_seq != rte_be_to_cpu_32( tcp->sent_seq ) )
	{
		rte_rwlock_write_unlock( &reassemble_packets[ index ].lock );
		return TCP_REASSLEMBLE_WRONG_SEQ;
	}

	memcpy( &reassemble_packets[ index ].data[ reassemble_packets[ index ].offset ], payload, size );
	RTE_LOG( DEBUG, WORKER, "pkt text: %s\n", reassemble_packets[ index ].data );
	reassemble_packets[ index ].offset += size;
	reassemble_packets[ index ].next_seq += size;
	rte_rwlock_write_unlock( &reassemble_packets[ index ].lock );
	return TCP_REASSLEMBLE_SUCCESS;
}

int tcp_reassemble_append_http( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size )
{
	RTE_LOG( DEBUG, WORKER, "Entering tcp_reassemble_append_http\n" );
	RTE_LOG( DEBUG, WORKER, "Trying to reassemble pkt\n" );
	struct tcp_flow key;
	key.src_ip = ipv4->src_addr;
	key.dst_ip = ipv4->dst_addr;
	key.src_port = tcp->src_port;
	key.dst_port = tcp->dst_port;
	int64_t index = 0;

	rte_rwlock_write_lock( &tcp_reass_conf.lock );
	int ret = rte_hash_lookup_data( tcp_reass_conf.flows, &key, (void**)&index );

	if( ret < 0 )
	{
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		return TCP_REASSLEMBLE_NOT_FOUND;
	}
	RTE_LOG( DEBUG, WORKER, "Found flow %ld\n", index );

	RTE_LOG( DEBUG, WORKER, "LOCK on %ld\n", index );
	RTE_LOG( DEBUG, WORKER, "CMP for seq %" PRIu32" and %"  PRIu32 "\n", reassemble_packets[ index ].next_seq, rte_be_to_cpu_32( tcp->sent_seq ) );
	if( reassemble_packets[ index ].next_seq != rte_be_to_cpu_32( tcp->sent_seq ) )
	{
		RTE_LOG( DEBUG, WORKER, "LOCK off %ld\n", index );
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		return TCP_REASSLEMBLE_WRONG_SEQ;
	}

	if( ( reassemble_packets[ index ].offset + size ) >= TCP_PKT_MAX_SIZE )
	{
		RTE_LOG( DEBUG, WORKER, "Bucket %ld is overflowed.\n", index );
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		tcp_reassemble_clean( index );
		return TCP_REASSLEMBLE_WRONG_SEQ;
	}

	memcpy( &reassemble_packets[ index ].data[ reassemble_packets[ index ].offset ], payload, size );
	RTE_LOG( DEBUG, WORKER, "Bucket data%s\n", reassemble_packets[ index ].data );
	reassemble_packets[ index ].offset += size;
	reassemble_packets[ index ].next_seq += size;
	if( memcmp(  &reassemble_packets[ index ].data[ reassemble_packets[ index ].offset - 4 ], "\r\n\r\n", 4 ) == 0 ){
		RTE_LOG( DEBUG, WORKER, "Got full HTTP GET: %s\n", reassemble_packets[ index ].data );
		RTE_LOG( DEBUG, WORKER, "LOCK off %ld\n", index );
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		return index;
	}
	RTE_LOG( DEBUG, WORKER, "LOCK off %ld\n", index );
	rte_rwlock_write_unlock( &tcp_reass_conf.lock );
	return TCP_REASSLEMBLE_SUCCESS;
}

int tcp_reassemble_append_https( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size )
{
	RTE_LOG( DEBUG, WORKER, "Entering tcp_reassemble_append_https\n" );
	RTE_LOG( DEBUG, WORKER, "Trying to reassemble pkt\n" );
	struct tcp_flow key;
	key.src_ip = ipv4->src_addr;
	key.dst_ip = ipv4->dst_addr;
	key.src_port = tcp->src_port;
	key.dst_port = tcp->dst_port;
	int64_t index = 0;

	rte_rwlock_write_lock( &tcp_reass_conf.lock );
	int ret = rte_hash_lookup_data( tcp_reass_conf.flows, &key, (void**)&index );

	if( ret < 0 )
	{
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		return TCP_REASSLEMBLE_NOT_FOUND;
	}
	RTE_LOG( DEBUG, WORKER, "Found flow %ld\n", index );

	RTE_LOG( DEBUG, WORKER, "CMP for seq %" PRIu32" and %"  PRIu32 "\n", reassemble_packets[ index ].next_seq, rte_be_to_cpu_32( tcp->sent_seq ) );
	if( reassemble_packets[ index ].next_seq != rte_be_to_cpu_32( tcp->sent_seq ) )
	{
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		return TCP_REASSLEMBLE_WRONG_SEQ;
	}

	if( ( reassemble_packets[ index ].offset + size ) >= TCP_PKT_MAX_SIZE )
	{
		RTE_LOG( DEBUG, WORKER, "Bucket %ld is overflowed.\n", index );
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		tcp_reassemble_clean( index );
		return TCP_REASSLEMBLE_WRONG_SEQ;
	}

	memcpy( &reassemble_packets[ index ].data[ reassemble_packets[ index ].offset ], payload, size );
	RTE_LOG( DEBUG, WORKER, "Bucket data%s\n", reassemble_packets[ index ].data );
	reassemble_packets[ index ].offset += size;
	reassemble_packets[ index ].next_seq += size;
	struct tls_recordlayer *tls_rl = ( struct tls_recordlayer * )reassemble_packets[ index ].data;
//	if( reassemble_packets[ index ].offset < sizeof( struct tls_recordlayer ) )
	if( reassemble_packets[ index ].offset <= 5 )
	{
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		tcp_reassemble_clean( index );
		return TCP_REASSLEMBLE_SUCCESS;
	}
	RTE_LOG( DEBUG, WORKER, "CMP len for %d and %lu\n", rte_be_to_cpu_16( tls_rl->length ), reassemble_packets[ index ].offset - sizeof( struct tls_recordlayer ) );
	if( ( reassemble_packets[ index ].offset - sizeof( struct tls_recordlayer ) ) >= (uint16_t)rte_be_to_cpu_16( tls_rl->length ) )
	{
		RTE_LOG( DEBUG, WORKER, "Got full HTTPS CLIENTHELLO: %s\n", reassemble_packets[ index ].data );
		rte_rwlock_write_unlock( &tcp_reass_conf.lock );
		tcp_reassemble_clean( index );
		return index;
	}
	rte_rwlock_write_unlock( &tcp_reass_conf.lock );
	return TCP_REASSLEMBLE_SUCCESS;
}

int tcp_reassemble_callback( int (*func)( char *, int ), int ret )
{
	RTE_LOG( DEBUG, WORKER, "Entering tcp_reassemble_callback\n" );
	RTE_LOG( DEBUG, WORKER, "Calling a callback for data %s\n", reassemble_packets[ ret ].data );
	if( ret < 0 )
	{
		return UNCLASSIFIED;
	}
	rte_rwlock_write_lock( &tcp_reass_conf.lock );
	int result =  func( reassemble_packets[ ret ].data, reassemble_packets[ ret ].offset );
	rte_rwlock_write_unlock( &tcp_reass_conf.lock );
	RTE_LOG( DEBUG, WORKER, "Callback result is %d\n", result );

	return result;
}

int64_t tcp_reassemble_create( struct ipv4_hdr * ipv4, struct tcp_hdr * tcp, char * payload, int size )
{
	RTE_LOG( DEBUG, WORKER, "Entering tcp_reassemble_create\ntcp seq: %d ; len: %d\n", rte_be_to_cpu_32( tcp->sent_seq ), size );
	if( size < 1 )
		return 0;
	struct tcp_flow key;
	key.src_ip = ipv4->src_addr;
	key.dst_ip = ipv4->dst_addr;
	key.src_port = tcp->src_port;
	key.dst_port = tcp->dst_port;

	rte_rwlock_write_lock( &tcp_reass_conf.lock );

	if( tcp_reass_conf.iterate < ( TCP_MAX_FLOWS - 1 ) )
		tcp_reass_conf.iterate++;
	else
		tcp_reass_conf.iterate = 0;

	int64_t ret = tcp_reass_conf.iterate;
	//rte_rwlock_write_lock( &reassemble_packets[ ret ].lock );

	if( reassemble_packets[ ret ].key.src_ip != 0 )
	{
		rte_hash_del_key( tcp_reass_conf.flows, &reassemble_packets[ ret ].key );
		reassemble_packets[ ret ].key.src_ip = 0;
		reassemble_packets[ ret ].key.dst_ip = 0;
		reassemble_packets[ ret ].key.src_port = 0;
		reassemble_packets[ ret ].key.dst_port = 0;
	}
	int add_res = rte_hash_add_key_data( tcp_reass_conf.flows, &key, (void*)ret );

	memset( &reassemble_packets[ ret ].data, 0, TCP_PKT_MAX_SIZE );
	memcpy( reassemble_packets[ ret ].data, payload, size );
	reassemble_packets[ ret ].key = key;
	reassemble_packets[ ret ].offset = size;
	reassemble_packets[ ret ].next_seq = rte_be_to_cpu_32( tcp->sent_seq ) + size;
	//rte_rwlock_write_unlock( &reassemble_packets[ ret ].lock );
	RTE_LOG( INFO, WORKER, "Created bucket %ld with result %d\n", ret, add_res );
	rte_rwlock_write_unlock( &tcp_reass_conf.lock );
	return ret;
}

void tcp_reassemble_clean( int64_t ret )
{
	RTE_LOG( DEBUG, WORKER, "Entering tcp_reassemble_clean\n" );
	RTE_LOG( DEBUG, WORKER, "LOCK on %ld\n", ret );
	rte_rwlock_write_lock( &tcp_reass_conf.lock );
	int d = rte_hash_del_key( tcp_reass_conf.flows, &reassemble_packets[ ret ].key );
	reassemble_packets[ ret ].key.src_ip = 0;
	reassemble_packets[ ret ].key.dst_ip = 0;
	reassemble_packets[ ret ].key.src_port = 0;
	reassemble_packets[ ret ].key.dst_port = 0;
	RTE_LOG( DEBUG, WORKER, "Clearing bucket %ld with ret %d\n", ret, d );
	rte_rwlock_write_unlock( &tcp_reass_conf.lock );
	RTE_LOG( DEBUG, WORKER, "LOCK off %ld\n", ret );
}
