struct node // структура для представления узлов дерева
{
        char *key;
        uint32_t len;
        unsigned char height;
        struct node* left;
        struct node* right;
};

struct node* avl_insert( struct node** p, char *k );
struct node* avl_lookup( struct node* p, char *k );
struct node* avl_lookup_trim( struct node* p, char *k );
void avl_remove_tree( struct node** n );
void avl_print_node( struct node* n );
