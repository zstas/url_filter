struct registry_entry
{
	char *hostname;
//	struct list *urls;
	struct node *urls;
	uint8_t block_http;
	uint8_t block_https;
	uint8_t totally_blocked;
	uint8_t wildcard_blocked;
};

void regentry_init( struct registry_entry **r, char *h, size_t length );
void regentry_add_url( struct registry_entry *r, char *u );
void regentry_print( struct registry_entry *r );
void regentry_flush( struct registry_entry **r );
void make_totally_blocked( struct registry_entry **r );
void make_wildcard_blocked( struct registry_entry **r );
void regentry_block_https( struct registry_entry **r );

