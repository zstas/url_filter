#include "main.h"

uint8_t mac_address[6];

static int tap_create( char *name )
{
	struct ifreq ifr;
	int fd, ret;

	fd = open( "/dev/net/tun", O_RDWR );
	if (fd < 0)
		return fd;

	memset( &ifr, 0, sizeof( ifr ) );

	/* TAP device without packet information */

	ifr.ifr_flags = IFF_TAP | IFF_NO_PI;
	if( name && *name )
		snprintf( ifr.ifr_name, IFNAMSIZ, "%s", name );

	ret = ioctl( fd, TUNSETIFF, (void *) &ifr );

	int skfd = socket( PF_INET, SOCK_DGRAM, IPPROTO_IP );
	if (skfd < 0 )
		return skfd;

	ret = ioctl( skfd, SIOCGIFFLAGS, (void*) &ifr );
	if ( ret < 0 )
		return ret;

	ifr.ifr_flags |= IFF_UP | IFF_RUNNING;
	ret = ioctl( skfd, SIOCSIFFLAGS, (void*) &ifr );

	if (ret < 0)
	{
		close(fd);
		return ret;
	}

	if( name )
	{
		snprintf( name, IFNAMSIZ, "%s", ifr.ifr_name );
	}

	ioctl( fd, SIOCGIFHWADDR, &ifr );

	rte_memcpy( mac_address, ifr.ifr_hwaddr.sa_data, 6 );

	RTE_LOG( DEBUG, LCORE_TAP , "MAC is %x:%x:%x:%x:%x:%x\n", mac_address[0], mac_address[1], mac_address[2], mac_address[3], mac_address[4], mac_address[5] );
	return fd;
}

__attribute__((noreturn)) void
lcore_tap( __attribute__((unused)) void *args_ptr )
{
	RTE_LOG( DEBUG, LCORE_TAP , "Launching lcore_tap...\n");
	char name[ IFNAMSIZ ] = "vEth0";
	int tap_fd = tap_create( name );

	char buf[128] = "tap_tx";
	struct rte_ring *ring;

	int i, rx_pkts, ret;
	void *packets[32];
	struct rte_mbuf *m;

	struct ether_hdr *eth_hdr;

	ring = rte_ring_create( buf, 4096, rte_socket_id(), RING_F_SC_DEQ );

	while( 1 )
	{
		if( likely( rte_ring_count( ring ) > 0 ) )
		{
			rx_pkts = (uint16_t)RTE_MIN( rte_ring_count( ring ), (unsigned)32 );
			rte_ring_dequeue_bulk( ring, packets, rx_pkts, NULL ) ;

			for( i = 0; i < rx_pkts; i++ )
			{
				m = packets[ i ];

				eth_hdr = rte_pktmbuf_mtod( m, struct ether_hdr * );
				rte_memcpy( eth_hdr->d_addr.addr_bytes, mac_address, 6 );

				ret = write( tap_fd, rte_pktmbuf_mtod( m, void* ), rte_pktmbuf_data_len( m ) );
				if( ret > 0 )
				{
					RTE_LOG(INFO, LCORE_TAP , "Sending pkt to tap interface\n" );
				}
				rte_pktmbuf_free( m );
			}
		}
	}
}

