
#include "main.h"

extern struct app_config global_config;

__thread __typeof__( struct registry_entry** ) http_registry;
__thread __typeof__( struct rte_hash* ) ipv4_registry_hash;
__thread __typeof__( struct rte_hash* ) sslhost_registry_hash;
__thread __typeof__( struct rte_hash* ) http_registry_hash;
__thread __typeof__( struct rte_hash* ) watchlist_hash;
__thread __typeof__( char ) text[4096];
__thread __typeof__( char ) host[4096];
__thread __typeof__( char )  uri[4096];
__thread __typeof__( struct ether_hdr * ) eth_hdr;
__thread __typeof__( struct vlan_hdr * ) vlan_hdr;
__thread __typeof__( struct ipv4_hdr * ) ipv4;
__thread __typeof__( struct ipv6_hdr * ) ipv6;
__thread __typeof__( struct tcp_hdr * ) tcp;
__thread __typeof__( int ) worker_queue_tx;
__thread __typeof__( int ) worker_queue_rx;
__thread __typeof__( uint8_t ) status;
__thread __typeof__( uint8_t ) worker_id;

__thread __typeof__( struct tls_recordlayer* ) tls_rl;
__thread __typeof__( struct tls_handshake *) hs;
__thread __typeof__( struct tls_handshake_sessionid * ) sid;
__thread __typeof__( struct tls_handshake_ciphersuite * ) cs;
__thread __typeof__( struct tls_handshake_compressionmethods * ) cm;
__thread __typeof__( struct tls_handshake_extensions * ) exs;
__thread __typeof__( struct tls_handshake_extension_entry *) exe;
__thread __typeof__( struct tls_extension_sni * ) sni;


static inline char* strip_domain( char* domain )
{
	uint8_t dots = 0;
	uint16_t i = 0;

	while( domain[i] )
	{
		if( domain[i] == '.' )
		{
			dots += 1;
		}
		i += 1;
	}

	if( dots <= 1 )
		return NULL;

	char *res = strchr( domain, '.' );
	res += 1;

	RTE_LOG( DEBUG, WORKER, "Stripped hostname is %s\n", res );
	return res;
}

static inline int check_http_url( char *hostname, char *uri )
{
	char *dom;
	int32_t res = rte_hash_lookup( http_registry_hash, hostname );
	RTE_LOG( DEBUG, WORKER, "Lookup for hostname %s with result %d\n", hostname, res );
	if( res < 0 )
	{
		dom = hostname;
		do
		{
			dom = strip_domain( dom );

			if( dom == NULL )
				return PASS;

			res = rte_hash_lookup( http_registry_hash, dom );
			RTE_LOG( DEBUG, WORKER, "For host %s hash lookup is %d\n", dom, res );
			if( res >= 0 )
			{
				struct registry_entry* re = http_registry[ res ];
				if( re->wildcard_blocked == 1 )
					return REDIRECT;
			}
		}
		while( dom );
	}

	if( res < 0 )
		return PASS;

	struct registry_entry* re = http_registry[ res ];

	if( re->totally_blocked == 1 )
	{
		RTE_LOG( DEBUG, WORKER, "Hostname %s is totally blocked\n", hostname );
		return REDIRECT;
	}

	RTE_LOG( DEBUG, WORKER, "ID%d Start lookuping urls for hostname %s\n", worker_id, hostname );

	struct node *lkp = avl_lookup_trim( re->urls, uri );
	if( lkp != NULL )
	{
		RTE_LOG( DEBUG, WORKER, "Found %s for %s\n", lkp->key, uri );
		return REDIRECT;
	}

	return PASS;
}

static inline int parse_http( char *payload, int len )
{
	RTE_LOG( DEBUG, WORKER , "Enter parsing func, len is %d\n", len );
	if( len < 1 )
		return PASS;

	if( len > 4095 )
		len = 4095;

	memset( text, 0, sizeof( text ) );

	//RTE_LOG(INFO, WORKER , "%.*x\n", len, payload );

	rte_memcpy( text, payload, len );
	text[ len ] = 0x0;

	if( memcmp( text, "GET ", ( len < 4 ) ? len : 4 ) == 0 )
	{
		char *begin = NULL;
		char *end = NULL;
		memset( uri, 0, sizeof( uri ) );
		end = strstr( text, " HTTP/" );

		if( end == NULL )
			return CLASS_HTTP_GET_REASSEMBLE;

		if( ( end - text ) < 5 )
			return CLASS_HTTP_GET_REASSEMBLE;

		rte_memcpy( uri, text + 4, end - text - 4 );

		memset( host, 0, sizeof( host ) );
		begin = strstr( text, "ost:" );

		if( begin == NULL )
		{
//			RTE_LOG(INFO, WORKER , "Get found: %s, but host is not found\n", uri );
//			RTE_LOG(INFO, WORKER , "Full request is below\n%s\n", text );
			return CLASS_HTTP_GET_REASSEMBLE;
		}

		begin += 4;

		if( *begin == ' ' )
			begin += 1;

		end = strstr( begin, "\n" );

		if( end == NULL )
			return CLASS_HTTP_GET_REASSEMBLE;

		if( *(end-1) == '\r' )
			end -= 1;

		if( *(end-1) == '.' )
			end -= 1;

		char *n_end = NULL;
		n_end = strstr( begin, ":" );
		if( ( n_end != NULL ) && ( unlikely( n_end < end ) ) )
		{
			end = n_end;
		}

		rte_memcpy( host, begin, end - begin );

		RTE_LOG( DEBUG, WORKER , "Parsed! host: %s\n", host );
		return CLASS_HTTP_GET;
	}
	return UNCLASSIFIED;
}

static inline int parse_https( char *payload, int len )
{
	RTE_LOG( DEBUG, WORKER, "Start parsing TLS packet with len %d\n", len );
	if( len < 1 )
		return PASS;

	tls_rl = ( struct tls_recordlayer * )payload;

	if( tls_rl->content_type != TLS_HANDSHAKE )
	{
		return UNCLASSIFIED;
	}
	if( sizeof( struct tls_recordlayer ) > ( uint )len )
	{
		return CLASS_HTTPS_REASSEMBLE;
	}

	RTE_LOG( DEBUG, WORKER, "TLS content type is: 0x%x ; version is 0x%x ; length is %d\n", tls_rl->content_type, tls_rl->version, tls_rl->length );

	if( rte_be_to_cpu_16( tls_rl->version ) < 0x301 )
	{
		return UNCLASSIFIED;
	}

	len -= sizeof( struct tls_recordlayer );

	if( rte_be_to_cpu_16( tls_rl->length ) > len )
	{
		return CLASS_HTTPS_REASSEMBLE;
	}

	RTE_LOG( DEBUG, WORKER , "len is %d and tls sub_len is %d\n", len, rte_be_to_cpu_16( tls_rl->length ) );
	hs = ( struct tls_handshake * )( payload + sizeof( struct tls_recordlayer ) );
	len -= sizeof( struct tls_handshake );
	if( len <= 0 )
		return TLS_HANDSHAKE_CLIENTHELLO;
	if( ( hs->handshake_type != TLS_HANDSHAKE_CLIENTHELLO ) )
	{
		return UNCLASSIFIED;
	}

	sid = ( struct tls_handshake_sessionid * )( payload + sizeof( struct tls_recordlayer ) + sizeof( struct tls_handshake ) );
	len -= sizeof( struct tls_handshake_sessionid ) + sid->length - 1;
	if( len <= 0 )
		return TLS_HANDSHAKE_CLIENTHELLO;
	cs = ( struct tls_handshake_ciphersuite * )( &sid->value + sid->length );
	len -= sizeof( struct tls_handshake_ciphersuite ) + rte_be_to_cpu_16( cs->length ) - 1;
	if( len <= 0 )
		return TLS_HANDSHAKE_CLIENTHELLO;
	cm = ( struct tls_handshake_compressionmethods * )( &cs->value + rte_be_to_cpu_16( cs->length ) );
	len -= sizeof( struct tls_handshake_compressionmethods ) + cm->length - 1;
	if( len <= 0 )
		return TLS_HANDSHAKE_CLIENTHELLO;
	exs = ( struct tls_handshake_extensions *)( &cm->value + cm->length );
	len -= sizeof( struct tls_handshake_extensions );
	if( len <= 0 )
		return TLS_HANDSHAKE_CLIENTHELLO;
	int cur_len = len;
	exe = ( struct tls_handshake_extension_entry * )( &exs->value );
	while( cur_len > 0 )
	{
		RTE_LOG( DEBUG, WORKER , "Current length %d; Extension type 0x%x, length %d\n", cur_len, rte_be_to_cpu_16( exe->type ), rte_be_to_cpu_16( exe->length ) );
		if( rte_be_to_cpu_16( exe->type ) == TLS_EXTENSION_SNI )
		{
			memset( host, 0, sizeof( host ) );
			sni = ( struct tls_extension_sni * )( &exe->value );
			RTE_LOG( DEBUG, WORKER , "sni list length %d, type %x, length %d\n", rte_be_to_cpu_16( sni->list_length ), sni->sni_type, rte_be_to_cpu_16( sni->sni_length ) );
			if( rte_be_to_cpu_16( sni->list_length ) == 0 )
				return TLS_HANDSHAKE_CLIENTHELLO;
			if( cur_len <= rte_be_to_cpu_16( sni->sni_length ) )
				return TLS_HANDSHAKE_CLIENTHELLO;
			rte_memcpy( host, ( char* )( &( sni->server_name ) ), rte_be_to_cpu_16( sni->sni_length ) );
			RTE_LOG( DEBUG, WORKER , "HTTPS SNI: https://%s\n", host );
			return CLASS_HTTPS_CLIENT_HELLO_SNI;
		}
		RTE_LOG( DEBUG, WORKER , "Current length is %d\n", cur_len );
		cur_len -= rte_be_to_cpu_16( exe->length ) + sizeof( struct tls_handshake_extension_entry ) - 1;
		exe = ( struct tls_handshake_extension_entry *)( &exe->value + rte_be_to_cpu_16( exe->length ) );
	}

	return CLASS_HTTPS_CLIENT_HELLO;
}

void create_hashes( void )
{
        http_registry = rte_malloc( NULL, MAX_REGISTRY_ENTRY * sizeof( void * ), 0 );

        int i;
        for( i = 0; i < MAX_REGISTRY_ENTRY; i += 1 )
        {
                http_registry[ i ] = NULL;
        }
        //memset( http_registry, 0, MAX_REGISTRY_ENTRY );

        char s[64];
        snprintf( s, sizeof( s ), "http_registry_%d", worker_id );

        struct rte_hash_parameters hash_params =
        {
                .name = s,
                .entries = 512000,
                .key_len = 1024,
                .hash_func_init_val = 0,
//		.hash_func = rte_hash_crc
        };

        http_registry_hash = rte_hash_create( &hash_params );

        snprintf( s, sizeof( s ), "ipv4_registry_%d", worker_id );
        hash_params.name = s;
        hash_params.key_len = 4;
	ipv4_registry_hash = rte_hash_create( &hash_params );

        snprintf( s, sizeof( s ), "sslhost_registry_%d", worker_id );
        hash_params.name = s;
	sslhost_registry_hash = rte_hash_create( &hash_params );


	snprintf( s, sizeof( s ), "watchlist_%d", worker_id );
	hash_params.name = s;
	hash_params.key_len = 1024;
	hash_params.entries = 1024;
	watchlist_hash = rte_hash_create( &hash_params );
}

void setup_hash( const char *f, const char *f2, const char *f3 )
{
        int i;
        for( i = 0; i < MAX_REGISTRY_ENTRY; i += 1 )
        {
                regentry_flush( &( http_registry[ i ] ) );
                http_registry[ i ] = NULL;
        }

        rte_hash_reset( ipv4_registry_hash );
        rte_hash_reset( sslhost_registry_hash );
        rte_hash_reset( http_registry_hash );
	rte_hash_reset( watchlist_hash );

        loader_init( f );
        sslhost_load( f2 );
	watchlist_load( f3 );
}


int classify_pkt( struct rte_mbuf *m )
{
	uint16_t ethertype;
	int len;

	rte_prefetch0( rte_pktmbuf_mtod( m, void * ) );
	eth_hdr = rte_pktmbuf_mtod( m, struct ether_hdr * );

	uint8_t vlan_offset = 0;

	ethertype = rte_be_to_cpu_16( eth_hdr->ether_type );

	if( ethertype == ETHER_TYPE_VLAN )
	{
		vlan_offset += 4;

		vlan_hdr = (struct vlan_hdr *)(eth_hdr + 1);
		ethertype = rte_be_to_cpu_16( vlan_hdr->eth_proto );


		if( ( rte_cpu_to_be_16( vlan_hdr->vlan_tci ) & 0xFFF ) == global_config.vlan_in )
		{
			vlan_hdr->vlan_tci = rte_cpu_to_be_16( ( rte_be_to_cpu_16( vlan_hdr->vlan_tci ) & 0xF000 ) + global_config.vlan_out );
		}
		else
		{
			if( ( rte_cpu_to_be_16( vlan_hdr->vlan_tci ) & 0xFFF ) == global_config.vlan_out )
			{
				vlan_hdr->vlan_tci = rte_cpu_to_be_16( ( rte_be_to_cpu_16( vlan_hdr->vlan_tci ) & 0xF000 ) + global_config.vlan_in );
			}
		}

		if( ethertype == ETHER_TYPE_VLAN )
		{
			vlan_offset += 4;

			vlan_hdr = (struct vlan_hdr *)(eth_hdr + 2);
			ethertype = rte_be_to_cpu_16( vlan_hdr->eth_proto );
		}
	}
	ipv4 = NULL;
	ipv6 = NULL;

	switch( ethertype )
	{
		case ETHER_TYPE_IPv4:
			ipv4 = rte_pktmbuf_mtod_offset( m, struct ipv4_hdr *, sizeof(struct ether_hdr) + vlan_offset );
			uint32_t dst_ip = rte_be_to_cpu_32( ipv4->dst_addr );

			if( rte_hash_lookup( ipv4_registry_hash, &dst_ip ) >= 0 )
			{
				return DROP;
			}

			if( ipv4->next_proto_id == IPPROTO_TCP )
			{
				tcp = rte_pktmbuf_mtod_offset( m, struct tcp_hdr *, sizeof(struct ether_hdr) + vlan_offset + ( ( ipv4->version_ihl & 0xF ) << 2 ) );

				char *payload = (char *)tcp + ( ( tcp->data_off & 0xF0 ) >> 2 );
				//len = m->data_len - ( sizeof(struct ether_hdr) + vlan_offset + ( ( ipv4->version_ihl & 0xF ) << 2 ) + ( ( tcp->data_off & 0xF0 ) >> 2 ) );
				len = rte_be_to_cpu_16( ipv4->total_length ) - ( ( ( ipv4->version_ihl & 0xF ) << 2 ) + ( ( tcp->data_off & 0xF0 ) >> 2 ) );
				//RTE_LOG( DEBUG, WORKER , "Got pkt frame len %d; vlan_offset %d; ip len %d; tcp len %d; calculated len: %d\n", m->data_len, vlan_offset, ( ipv4->version_ihl & 0xF ) << 2 ,  ( tcp->data_off & 0xF0 ) >> 2 , len );

				int res = parse_http( payload, len );
				int64_t ret = 0;

				switch( res )
				{
					case CLASS_HTTP_GET:
						to_lower( host );

						if( rte_hash_lookup( watchlist_hash, host ) >= 0 )
						{
							//RTE_LOG( INFO, WORKER, "ID%d " IPv4_BYTES_FMT ":%d:" IPv4_BYTES_FMT ":%d -> Watchlist %s\n", worker_id, IPv4_BYTES( rte_be_to_cpu_32( ipv4->src_addr ) ), rte_be_to_cpu_16( tcp->src_port ), IPv4_BYTES( rte_be_to_cpu_32( ipv4->dst_addr ) ), rte_be_to_cpu_16( tcp->dst_port ), host );
						}

						return check_http_url( host, uri );
					case CLASS_HTTP_GET_REASSEMBLE:
						tcp_reassemble_create( ipv4, tcp, payload, len );
						return PASS;
					case UNCLASSIFIED:
						break;
						if( rte_be_to_cpu_16( tcp->dst_port ) == 80 )
						{
							if( tcp_reassemble_callback( parse_http, tcp_reassemble_append_http( ipv4, tcp, payload, len ) ) == CLASS_HTTP_GET )
							{
								tcp_reassemble_clean( ret );
								to_lower( host );
								return check_http_url( host, uri );
							}
						}
				}

				//if( m->data_len <= 64 )
				//	return PASS;
				res =  parse_https( payload, len );
				int32_t check = 0;

				switch( res )
				{
					case CLASS_HTTPS_REASSEMBLE:
						//tcp_reassemble_create( ipv4, tcp, payload, len );
						return PASS;
					case CLASS_HTTPS_CLIENT_HELLO:
						check = rte_hash_lookup( sslhost_registry_hash, &dst_ip );
						RTE_LOG( DEBUG, WORKER , "SSL host hash check with result %d\n", check );
						if( check >= 0 )
						{
							return RESET_IP;
						}
						return PASS;
						break;
					case CLASS_HTTPS_CLIENT_HELLO_SNI:
						host[ 4095 ] = 0x0;
						to_lower( host );

						if( rte_hash_lookup( watchlist_hash, host ) >= 0 )
						{
							RTE_LOG( INFO, WORKER, "ID%d " IPv4_BYTES_FMT ":%d:" IPv4_BYTES_FMT ":%d -> Watchlist %s\n", worker_id, IPv4_BYTES( rte_be_to_cpu_32( ipv4->src_addr ) ), rte_be_to_cpu_16( tcp->src_port ), IPv4_BYTES( rte_be_to_cpu_32( ipv4->dst_addr ) ), rte_be_to_cpu_16( tcp->dst_port ), host );
						}

						check = rte_hash_lookup( http_registry_hash, host );
						RTE_LOG( DEBUG, WORKER , "HTTPS hash check with result %d\n", check );
						if( check >= 0 )
						{
							if( http_registry[ check ]->block_https == 1 )
								return RESET;
						}
						return PASS;
						break;
					case UNCLASSIFIED:
						RTE_LOG( DEBUG, WORKER , "HTTPS reassemble check\n" );
						return PASS;
						if( rte_be_to_cpu_16( tcp->dst_port ) == 443 )
						{
							RTE_LOG( DEBUG, WORKER , "HTTPS reassemble start\n" );
							if( tcp_reassemble_callback( parse_https, tcp_reassemble_append_https( ipv4, tcp, payload, len ) ) == CLASS_HTTPS_CLIENT_HELLO_SNI )
							{
								host[ 4095 ] = 0x0;
								to_lower( host );
								check = rte_hash_lookup( http_registry_hash, host );
								RTE_LOG( DEBUG, WORKER , "HTTPS hash check with result %d\n", check );
								if( check >= 0 )
								{
									if( http_registry[ check ]->block_https == 1 )
										return RESET;
								}
							}
						}
						break;
				}

				return PASS;
			}
			break;
		case ETHER_TYPE_IPv6:
			ipv6 = rte_pktmbuf_mtod_offset( m, struct ipv6_hdr *, sizeof(struct ether_hdr) + vlan_offset );

			if( ipv6->proto == IPPROTO_TCP )
			{
				tcp = rte_pktmbuf_mtod_offset( m, struct tcp_hdr *, sizeof(struct ether_hdr) + vlan_offset + sizeof( struct ipv6_hdr ) );

				char *payload = (char *)tcp + ( ( tcp->data_off & 0xF0 ) >> 2 );
				//len = m->data_len - ( sizeof(struct ether_hdr) + vlan_offset + ( ( sizeof( struct ipv6_hdr ) ) << 2 ) + ( ( tcp->data_off & 0xF0 ) >> 2 ) );
				//len = rte_be_to_cpu_16( ipv4->total_length ) - ( ( ( ipv4->version_ihl & 0xF ) << 2 ) + ( ( tcp->data_off & 0xF0 ) >> 2 ) );
				len = rte_be_to_cpu_16( ipv6->payload_len ) - ( ( tcp->data_off & 0xF0 ) >> 2 );
				RTE_LOG( DEBUG, WORKER , "Got pkt frame len %d; vlan_offset %d; ip len %ld; tcp len %d; calculated len: %d\n", m->data_len, vlan_offset, sizeof( struct ipv6_hdr ) ,  ( tcp->data_off & 0xF0 ) >> 2 , len );
				RTE_LOG( DEBUG, WORKER , "Got v6 pkt with len %d:\n%s\n", len, payload );

				int res = parse_http( payload, len );

				switch( res )
				{
					case CLASS_HTTP_GET:
						to_lower( host );
						int check = check_http_url( host, uri );
						if( check == REDIRECT ) return REDIRECT_V6;
						return check;
					case UNCLASSIFIED:
						break;
				}

				res =  parse_https( payload, len );
				int32_t check = 0;

				switch( res )
				{
					case CLASS_HTTPS_CLIENT_HELLO:
						return PASS;
					case CLASS_HTTPS_CLIENT_HELLO_SNI:
						host[ 4095 ] = 0x0;
						to_lower( host );

						check = rte_hash_lookup( http_registry_hash, host );
						RTE_LOG( DEBUG, WORKER , "HTTPS hash check with result %d\n", check );
						if( check >= 0 )
						{
							if( http_registry[ check ]->block_https == 1 )
								return RESET;
						}
						return PASS;
						break;
					case UNCLASSIFIED:
						break;
				}

				return PASS;
			}
			break;
		default:
			return PASS;
	}

	return PASS;
}

