#define TLS_HANDSHAKE 0x16
#define TLS_HANDSHAKE_CLIENTHELLO 0x1
#define TLS_EXTENSION_SNI	0x0000

struct tls_recordlayer
{
	uint8_t content_type;
	uint16_t version;
	uint16_t length;
}__attribute__((packed,aligned(1)));

struct tls_handshake
{
	uint8_t handshake_type;
	uint8_t len_hi;
	uint16_t len_lo;
	uint16_t version;
	uint8_t random[32];
}__attribute__((packed,aligned(1)));

struct tls_handshake_sessionid
{
	uint8_t length;
	uint8_t value;
}__attribute__((packed,aligned(1)));

struct tls_handshake_ciphersuite
{
	uint16_t length;
	uint8_t value;
}__attribute__((packed,aligned(1)));

struct tls_handshake_compressionmethods
{
	uint8_t length;
	uint8_t value;
}__attribute__((packed,aligned(1)));

struct tls_handshake_extensions
{
	uint16_t length;
	uint8_t value;
}__attribute__((packed,aligned(1)));

struct tls_handshake_extension_entry
{
	uint16_t type;
	uint16_t length;
	uint8_t value;
}__attribute__((packed,aligned(1)));

struct tls_extension_sni
{
	uint16_t list_length;
	uint8_t sni_type;
	uint16_t sni_length;
	uint8_t server_name;
}__attribute__((packed,aligned(1)));
