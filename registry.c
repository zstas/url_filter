#include "main.h"

void regentry_init( struct registry_entry **r, char *h, size_t length )
{
	*r = ( struct registry_entry* )rte_malloc( "struct_registry", sizeof( struct registry_entry ), 0 );
	struct registry_entry* new_r = *r;
	new_r->hostname = ( char* )rte_malloc( "regentry_name", length + 1, 0 );
	strncpy( new_r->hostname, h, length );
	new_r->hostname[ length ] = 0x0;
	new_r->block_http = 0;
	new_r->block_https = 0;
	new_r->totally_blocked = 0;

	new_r->urls = NULL;
}

void make_totally_blocked( struct registry_entry **r )
{
        struct registry_entry* new_r = *r;
	new_r->totally_blocked = 1;
	avl_remove_tree( &(new_r->urls) );
}

void make_wildcard_blocked( struct registry_entry **r )
{
	struct registry_entry* new_r = *r;
	new_r->wildcard_blocked = 1;
	avl_remove_tree( &(new_r->urls) );
}

void regentry_block_https( struct registry_entry **r )
{
	struct registry_entry* new_r = *r;
	new_r->block_https = 1;
}

void regentry_add_url( struct registry_entry *r, char *u )
{
	if( r->totally_blocked == 1 )
	{
		//printf( "Hostname is totally blocked! Failed\n" );
		return;
	}
	r->urls = avl_insert( &( r->urls ), u );
}

void regentry_print( struct registry_entry *r )
{
	if( r == NULL )
		return;

	if( r->hostname == NULL )
		return;

	printf( "Registry entry for host: %s\n", r->hostname );
	if( r->totally_blocked == 1 )
	{
		printf( "Hostname is totally blocked\n" );
	}
	avl_print_node( r->urls );
}

void regentry_flush( struct registry_entry **r )
{
	if( r == NULL )
		return;

	struct registry_entry* new_r = *r;

	if( new_r == NULL )
		return;

	avl_remove_tree( &(new_r->urls) );
	rte_free( new_r->hostname );
	new_r->hostname = NULL;
	rte_free( *r );
	*r = NULL;
}

