struct list
{
        char *text;
	uint32_t len;
        struct list *next;
};

void init_list( struct list **nl, char *t, size_t length );
void insert( struct list **nl, char *t, size_t length );
void print_list( struct list *l );
void flush_list( struct list **nl );

